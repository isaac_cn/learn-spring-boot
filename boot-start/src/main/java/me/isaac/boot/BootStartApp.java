package me.isaac.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootStartApp {
    public static void main(String args[]) {
        SpringApplication.run(BootStartApp.class, args);
    }
}
